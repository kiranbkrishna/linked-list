#include<stdio.h>
#include<assert.h>
#include<stdlib.h>

struct node {
    int data;
    struct node *next;
};

void add(struct node **head, int data); // Add element at the begining of the list
void append(struct node **head, int data); // Add element at the end of the list
void appendAfter(struct node *head, struct node *ptr, int data); // Add element after elemet pointed by ptr
void deleteItem(struct node **head, int data); // Delete first occurance with data item data
void deleteAll(struct node *head, int data); // Delete all occourances of data in list
void printList(struct node * head); // Print the items stored in list from head
int length(struct node **head); // Returns the number of items in the list
int count(struct node **headptr, int find); // Returns the number of times find occures in a list
int getNth( struct node **headptr, int index); // Returns the data at list iten index. Counting starts from zero.
void deleteList( struct node ** headptr); //Dealloctes all nodes in the list and set head to null
int pop( struct node **headptr); // Deletes the element pointed to by the head and return its data
void insertNth( struct node **headptr, int n, int data); // Inserts a new node at position n of thw list
void sortedInsert( struct node **headptr, int data); // Inserts data into proper posiion in an ascendigly sorted order
void insertSort(struct node **headptr, int data); // Sorts the list and adds node with data into proper position in the list
void appendLists(struct node **headptrA, struct node **headptrB); // Appends list B at the end of list A
void frontBackSplit(struct node **headptr); // Split the list into two halves

struct node*  getBlock(int num)
{
    return (struct node*) calloc( (size_t) num, sizeof(struct node) );
}

void add(struct node  **head , int data)
{
    struct node* newBlock = getBlock(1);
    newBlock->data = data;
    assert(newBlock != NULL);
    if ( *head == NULL )
    {
        *head = newBlock;
        (*head)->next = NULL;
    }
    else
    {
        newBlock->next = *head;
        *head = newBlock;
    }
}

void append(struct node **head, int data)
{
    struct node *newBlock = getBlock(1);
    newBlock->data = data;
    newBlock->next = NULL;
    assert(newBlock != NULL);
    if (*head == NULL)
    {
        *head = newBlock;
    }
    else
    {
        struct node *i, *j;
        for (i = *head; i != NULL; i = i->next)
        {
            j = i;
        }
        j->next = newBlock;
    }
}

void appendAfter(struct node *head, struct node *ptr, int data)
{
    assert(ptr != NULL);
    struct node *newBlock = getBlock(1);
    newBlock->data = data;
    assert(newBlock !=  NULL);
    if (head == NULL)
    {
     head = newBlock;
     head->next = NULL;
    }
    else
    {
        struct node *i, *j;
        newBlock->next = ptr->next;
        ptr->next = newBlock;
    }
}

void deleteItem(struct node **head, int data)
{
    if (*head == NULL)
        return;
    struct node *i, *j = *head;
    for (i = *head; i->data != data; i = i->next)
        j = i;
    if (i == NULL)
    {
        printf("Delete item not found");
        return;
    }

    if (i == *head)
    {
        *head = i->next;
    }
    j->next = i->next;
    free(i);
}

void deleteAll(struct node *head, int data)
{
    struct node *i, *j;
    j = head;
    for (i = head; i != NULL; i = i->next)
    {
        if( i != NULL)
        {
            if (i->data == data)
                j->next = i->next;
            if (i == head)
               head = j;
            free(i);
        }
    }
}

void printList(struct node *head)
{
    if(head != NULL)
    {
        struct node *i;
        for (i = head; i != NULL; i = i->next)
            if (i->next != NULL)
                printf("%d ->", i->data);
           else
                printf ("%d", i->data);
    }
    else
    {
        printf("list is empty\n");
    }
    printf("\n");
}

int count (struct node **headptr, int find)
{
    struct node *i;
    int count = 0;
    for (i = *headptr; i != NULL; i = i->next)
    {
        if (i->data == find)
            ++count;
    }
    return count;
}

int length (struct node **headptr)
{
    if (*headptr == NULL)
    {
        return -1;
    }
    struct node *i;
    int len = 0;
    for (i = *headptr; i != NULL; i = i->next)
    {
        ++len;
    }
    return len;
}

int getNth (struct node **headptr, int index)
{
    if ((length(headptr) - 1) < index)
        return -1;
    struct node *j = *headptr;
    int i = index;
    while (i > 0)
    {
        --i;
        j = j->next;
    }
    if (j == NULL)
        return -1;
    return j->data;
}

void deleteList( struct node **headptr)
{
    struct node *tempArr[length(headptr)], *j;
    int i = 0;
    for ( j = *headptr; j != NULL; j = j->next )
    {
        tempArr[i++] = j;
    }

    while (i > 0)
    {
        free(tempArr[--i]);
    }
    *headptr = NULL;
}

int pop( struct node **headptr)
{
    int ret = (*headptr)->data;
    struct node *i = *headptr;
    if ( i != NULL)
        *headptr = i->next;
    return ret;
}

void insertNth( struct node **headptr, int n, int data)
{
    if (n > length(headptr))
    {
        printf ("Unable to insert at provided index\n");
        return;
    }
    struct node *i, *j;
    int index = 0;
    i = *headptr;
    while (index < n-1)
    {
        ++index;
       i = i->next;
    }
    j = getBlock(1);
    j->data = data;
    j->next = i->next;
    if (i == *headptr)
        *headptr = j;
    i->next = j;
}

void sortedInsert(struct node **headptr, int data)
{
    if (*headptr == NULL)
    {
        *headptr = (struct node *)getBlock(1);
        (*headptr)-> data = data;
        (*headptr)-> next = NULL;
        return;
    }
    struct node *i = NULL, *j = *headptr;
    for (i = *headptr;  i->data < data; i = i->next)
    {
        j = i;
    }
    struct node *newBlock = getBlock(1);
    newBlock ->data = data;
    if (i  == *headptr)
    {
        newBlock->next = *headptr;
        *headptr = newBlock;
    }
    else
    {
        newBlock->next = j->next;
        j->next = newBlock;
    }
}

void swap(struct node *a, struct node *b) // Helper function
{
   int  temp = a->data;
    a->data = b->data;
    b->data = temp;
}


void sort(struct node **headptr)
{
    struct node *i, *j;
    for (i = *headptr; i != NULL; i = i->next)
    {
        for (j = *headptr; j != NULL; j = j->next)
        {
            if (i->data < j->data)
                swap (i , j);
        }
    }
}

void insertSort(struct node **headptr, int data)
{
    sort(headptr);
    sortedInsert(headptr, data);
}

void appendLists(struct node **headptrA, struct node **headptrB)
{
    struct node *i, *j = *headptrA;
    for (i = *headptrA; i != NULL; i = i->next)
    {
       j = i;
    }
    j->next = *headptrB;
    headptrB = NULL;
}


void frontBackSplit(struct node **headptr)
{
    int len = length(headptr);
    if (len == 0 || len == 1)
        return;
    int count1, count2;
    struct node *i, *j, *li1, *li2;
    if (len % 2 == 0)
    {
        count1 = (int)(len / 2); 
        count2 = (int)(len / 2); 
    }
    else
    {
        count1 = (int)(len / 2) + 1;
        count2 = len - count1;
    }

    int index = count1;
    i = *headptr;
    while (index > 1)
    {
        --index;
        i = i->next; 
    }

    if (count2 > 0)
    {
        li2 = i->next;
        i->next = NULL;
    }
    li1 = *headptr; 
    printList(li1);
    printList(li2);

}
