#include "linked.h"

int main()
{
    struct node  *head = NULL;
    append(&head, 1);
    append(&head, 1);
    append(&head, 2);
    append(&head, 6);
    append(&head, 10);
    printList(head);
    sortedInsert(&head, 0);
    printList(head);
}
