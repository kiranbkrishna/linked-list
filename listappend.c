#include "linked.h"

int main()
{
    struct node *a = NULL, *b = NULL;
    append(&a, 1);
    append(&a, 2);
    append(&a, 3);
    append(&b, 1);
    append(&b, 2);
    append(&b, 3);
    printList(a);
    printList(b);
    appendLists(&a, &b);
    printList(a);
    printList(b);
}
