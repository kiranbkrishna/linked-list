#include<stdio.h>
#include<assert.h>
#include "linked.h"

void main()
{
    struct node *head;
    head = NULL;
    add(&head, 1);
    add(&head, 2);
    add(&head, 3);
    add(&head, 4);
    printList(head);
    append(&head, 4);
    add(&head, 5);
    append(&head, 5);
    printList(head);
    deleteItem(&head, 1);
    printList(head);

    printf("count of 5 in list = %d\n", count(&head, 5));
    printf("count of 2 in list = %d\n", count(&head, 2));
    printf("count of 1 in list = %d\n", count(&head, 1));
    
    printList(head);
    printf("Data at index 5 = %d\n", getNth(&head, 5));
    printf("Data at index 3 = %d\n", getNth(&head, 3));
    printf("Data at index 0 = %d\n", getNth(&head, 0));
    
    printList(head);
    
    printf(" pop operation result =  %d\n", pop(&head));
    printList(head);
    insertNth(&head, 2, 21);
    insertNth(&head, 0, 21);
    insertNth(&head, 100, 21);
    printList(head);
    printf("swaping...\n");
    swap(head, head->next);
    printList(head);
    printf("swaping...\n");
   
    printList(head);
    printf("insert sort 12\n");
    insertSort(&head, 12);
    printList(head);        
}
